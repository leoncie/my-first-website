
<?php 
@session_start();
?>
<?php require_once('Connections/connections.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "1";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  global $connections;
if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($connections,$theValue) : mysqli_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_User = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_User = $_SESSION['MM_Username'];
}

$query_User = sprintf("SELECT * FROM register WHERE username = %s", GetSQLValueString($colname_User, "text"));
$User = mysqli_query($connections,$query_User) or die(mysqli_error($connections));
$row_User = mysqli_fetch_assoc($User);
$totalRows_User = mysqli_num_rows($User);
?>



<!DOCTYPE html>
<html>
    <header>
        <meta name="vieweport" content="with= device-width , initial scale=1.0">
        <title>
            RWINKWAVU READY FOR READING
        </title>
        
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet"  href="https://stackpath.bootstracdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,400;1,100;1,400&display=swap" rel="stylesheet">
<style>
      .header{
         
          background-image: url('images/home.PNG');
          min-height: 80vh;
    width: 100%;
    
    background-size:cover;
    background-position: center;
 
     } 
     
</style>

</header>
</header>
    <body>
       
        <section class="header">
            <nav>
                <a href="index.html"><img src="images/log.PNG"></a>
                <div class="nav-links" id="navlinks">
                    <i class="fa fa-times" onclick="hideMenu" ></i>
                    <ul>
                        <li > <a href="index.html">HOME</a></li>
                        <li > <a href="about.html">ABOUT US</a></li>
                        <li > <a href="contact.html">CONTACT</a></li>
                        <li > <a href="location.html">LOCATION</a></li>
                        <li><a href="./logout.php">Logout</a></li>
                    </ul>

                </div>
                <i class="fa fa-bars" onclick="showMenu"></i>
            </nav>
            <div class="text-box">
<marquee><h1> Welcome,<?php echo $row_User['firstname']; ?>
 <?php echo $row_User['lastname']; ?> to Rwinkwavu Ready For Reading</h1>
    </marquee>
                <br>
                <!--<P style="color:red">
                Rwinkwavu ready for reading is the biggest good organization for every one  who need to read any kind of book he/she need.
                </P>-->
                <a href="contact.html" class="hero-btn"> Call us to know more </a>
            </div>

        </section>

        <!--training-->

        <section class="training">

            <h1>
                Some services we offer
            </h1>
            <p>
                The ready for reading teaching a computer,  speaking,readin and writting English<br> 
                and also teach the peoples who did not get a  chance of to study to ready and to write.
                
            </p>
            <div class="row">
                <div class="training_col">
                    <h3>
                        Certificate in computer
                    </h3>
                    <p >
                        The ready for reading give a Certificate 
                        to a people  who complete
                         months in learning of  using a computer.<br>
                         If you want to study a Computer  click on below botton to register.<br><br>
                         <a href="registration.php" class="hero-btn"> Register Here </a></p>
                        
                </div>
                 <div class="training_col">
                <h3>
                    Certificate in English
                </h3>
                <p>
                    The ready for reading give a Certificate to <br>
                     a people who complete six months in
                     learning of to Speaking, Reading and Writting English.
                     If you want to study an English  click on below botton to register.<br><br>
                        <a href="registration.php" class="hero-btn"> Register Here </a>
                </p>
                 </div>
                 <div class="training_col">
                    <h3>
                        Teaching Reading and Writting
                    </h3>
                    <p>
                        The ready for reading  teaching the  peoples  who did not <br>
                         get a chance of to study to ready and to write.
                         If you want to study a Reading and Writting  click on below botton to register.<br><br>
                          <a href="registration.php" class="hero-btn"> Register Here </a>
                        
                    </p>
                     </div>
            </div>
        </section>
       
        <!--campany-->

        <section class="campany"> 
            
            <h1>
                Our  department
            </h1>
            <p>
                This organization has different department because , It's offering  many different  services
            </p>
            
            <div class="row">
                 <div class="campany-col">
                 <img src= "images/L5.PNG">
                    <div class="layer">
                     <h3>
                        LIBRARY
                     </h3>
                    </div> 
                 </div> 
                 <div class="campany-col">
                    <img src= "images/class.PNG">
                       <div class="layer">
                        <h3>
                           CLASS
                        </h3>
                       </div> 
                    </div>   
                    <div class="campany-col">
                        <img src= "images/computer.PNG">
                           <div class="layer">
                            <h3>
                               COMPUTER LAB
                            </h3>
                           </div> 
                        </div>
                        <div class="campany-col">
                            <img src= "images/play.PNG">
                               <div class="layer">
                                <h3>
                                   PLAYGROUND
                                </h3>
                               </div> 
                            </div>     
            </div>     
               
             
        
        </section>

        <!--javascript to toggle menu-->
<script>
    nav navlinks=document.getElementById("navlinks");
    function nav-links(){
        navlinks.style.right="0";

    }
    function nav-links(){
        navlinks.style.right="-200px";
        
    }
</script>
<!--- footer--->
<section class="footer ">
    Designed by:Leoncie @
     2021

</section>
    </body>
</html>